package com.craftogether.poc.kafka.lister;

import com.salesforce.kafka.test.junit5.SharedKafkaTestResource;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Durations.TEN_SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class KafkaReader {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaReader.class);

    @RegisterExtension
    public static final SharedKafkaTestResource sharedKafkaTestResource = new SharedKafkaTestResource();

    public static final String TOPIC_NAME = "j-acad";
    public static final String GROUP_ID = "ST";
    public static final Integer NB_PARTITION = 5;

    @BeforeAll
    public static void initTopic() {
        // Create admin client
        try (final AdminClient adminClient = sharedKafkaTestResource.getKafkaTestUtils().getAdminClient()) {
            // Define topic
            final NewTopic newTopic = new NewTopic(TOPIC_NAME, NB_PARTITION, (short) 1);

            // Create topic, which is async call.
            final CreateTopicsResult createTopicsResult = adminClient.createTopics(Collections.singleton(newTopic));

            // Since the call is Async, Lets wait for it to complete.
            createTopicsResult.values().get(TOPIC_NAME).get();
        } catch (InterruptedException | ExecutionException e) {
            fail("Impossible de crée le topic", e);
        }
    }

    private KafkaConsumer<String, String> getConsumerReset(boolean reset) {
        var props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, sharedKafkaTestResource.getKafkaConnectString());
        props.put(KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(GROUP_INSTANCE_ID_CONFIG, GROUP_ID);
        props.put(AUTO_OFFSET_RESET_CONFIG, "earliest");

        var consumer = new KafkaConsumer<String, String>(props);

        // Via la lib de test
//        var consumer = sharedKafkaTestResource.getKafkaTestUtils()
//                .getKafkaConsumer(StringDeserializer.class, StringDeserializer.class);

        var partitionInfoList = consumer.partitionsFor(TOPIC_NAME);
        LOG.debug("consumer partitionInfoList: {}", partitionInfoList);
        var topicPartitionList = partitionInfoList.stream()
                .map(partitionInfo -> new TopicPartition(TOPIC_NAME, partitionInfo.partition()))
                .collect(Collectors.toList());
        LOG.debug("consumer partitionList: {}", topicPartitionList);

        consumer.assign(topicPartitionList);

        if (reset) {
            consumer.seekToBeginning(topicPartitionList);
        }

        return consumer;
    }

    private void produceRecords(int number) {
        // Get a producer
        try (final KafkaProducer<String, String> producer = sharedKafkaTestResource.getKafkaTestUtils()
                .getKafkaProducer(StringSerializer.class, StringSerializer.class)
        ) {

            // Produce number records
            for (int recordCount = 0; recordCount < number; recordCount++) {
                String msg = "Value " + recordCount;
                // Create a record.
                final ProducerRecord<String, String> record = new ProducerRecord<>(
                        TOPIC_NAME,
                        "Key " + recordCount,
                        msg
                );
                LOG.info("send message : {}", msg);
                producer.send(record);
            }
            // Ensure messages are flushed.
            producer.flush();
        } catch (Exception e) {
            fail("Impossible de produire de message ", e);
        }
    }

    @Test
    @Order(1)
    @DisplayName("Consummer present avant la production de massage")
    void startFrom0() {
        var consumer = getConsumerReset(false);

        await().atMost(TEN_SECONDS).untilAsserted(() -> {
            var records = consumer.poll(Duration.ofSeconds(1));
            assertEquals(0L, records.count());
        });

        produceRecords(5);

        await().atMost(TEN_SECONDS).untilAsserted(() -> {
            var records = consumer.poll(Duration.ofSeconds(1));
            assertEquals(5L, records.count());
        });

    }

    @Test
    @Order(2)
    @DisplayName("Nouveau consumer present apres la production de massage sans reset d'offset")
    void startFrom5() {
        var consumer = getConsumerReset(false);

        await().atMost(TEN_SECONDS).untilAsserted(() -> {
            var records = consumer.poll(Duration.ofSeconds(1));
            assertEquals(0L, records.count());
        });
    }

    @Test
    @Order(3)
    @DisplayName("Nouveau consumer present apres la production de massage avec reset d'offset")
    void restartFrom0() {
        var consumer = getConsumerReset(true);

        await().atMost(TEN_SECONDS).untilAsserted(() -> {
            var records = consumer.poll(Duration.ofSeconds(1));
            assertEquals(5L, records.count());
        });
    }
}
